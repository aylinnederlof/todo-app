import { List, ListIcon, ListItem, Box, Heading, Text, FormLabel, Input, InputGroup, InputRightElement, Button, IconButton, Icon, Flex, chakra } from "@chakra-ui/react";
// import { CheckCircleIcon, LinkIcon } from '@chakra-ui/icons'
import { MdCheckBoxOutlineBlank, MdCheckBox, MdDelete } from 'react-icons/md'
import { useState, useEffect } from "react";
import { IconType } from "react-icons";
import { v4 as uuidv4 } from 'uuid';



export function Todo() {
  const title = 'My To-do component!';

  interface Todo {
    todoId: string 
    title: string
    complete: boolean
  }

  const [toDoItems, setToDoItems] =  useState<Todo[]>([]);
  const [toDo, setToDo] =  useState<string>('');
  const [completedToDos, setCompletedToDos] =  useState<number>(0);
  const [uncompletedToDos, setUncompletedToDos] =  useState<number>(0);
  const handleInputChange = (e) =>{ 
    setToDo(e.target.value);
  }

  const addToDo = (event)=>{
    event.preventDefault();
    const newTodo: Todo = {todoId: uuidv4() , title:toDo, complete: false};
    setToDoItems(prev => [newTodo,...prev]);
    setToDo('');
  }

  const RemoveToDo = (clickedTodo: string) =>{
    const newTodos = [...toDoItems].filter(todo => todo.todoId !== clickedTodo)
    setToDoItems(newTodos);
  }
  
  // complete / uncomplete todo
  const markToDo = (clickedTodo: string) =>{
    const newTodos = [...toDoItems]
    newTodos.map((todo)=> todo.todoId === clickedTodo ? todo.complete = !todo.complete : todo.complete = todo.complete);
    // newTodos.sort((a,b) => (a.complete > b.complete) ? 1 : ((b.complete > a.complete) ? -1 : 0));
    setToDoItems(newTodos);
  }

  useEffect(() => {
    // show remaining and completed todo's
    setCompletedToDos([...toDoItems].filter(todo => todo.complete === true).length);
    setUncompletedToDos([...toDoItems].filter(todo => todo.complete === false).length);
  },[toDoItems]);


  return (
  <>
  <Box border='1px' borderColor='teal.500' borderRadius='md' padding={4}>
    <Heading size='lg' marginBottom={4}>{title}</Heading>
    {toDoItems.length > 0 &&
      <Flex justify='space-between' marginBottom={4}>
        <Text>Remaining To-do's: <chakra.span fontWeight='extrabold' color='teal.400'>{uncompletedToDos === 0 ? "All done!" : uncompletedToDos}</chakra.span></Text>
        {completedToDos === 0 ? <Text color='purple.400'>Start checking of these todo's!</Text> : <Text>Completed To-do's: <chakra.span fontWeight='extrabold' color='teal.400'>{completedToDos}</chakra.span></Text>}
      </Flex>
    }
    <form onSubmit={addToDo}>
      <FormLabel htmlFor='todoTitle' size='sm' marginBottom={2}>Add to-do</FormLabel>
      <InputGroup size='md' marginBottom={4}>
        <Input id="todoTitle" type='text' value={toDo} placeholder="new todo item" onChange={handleInputChange} focusBorderColor='purple.500'/>
        <InputRightElement width='auto' marginRight={2}>
          <Button bgColor='purple.500' h='1.75rem' size='sm' type='submit' isDisabled={!toDo ? true : false}>
            Add To-do
          </Button>
        </InputRightElement>
      </InputGroup>
    </form>
    <List spacing={2}>
      {toDoItems.map((todo) => {
        return (
          <ListItem key={todo.todoId} display="flex" justifyContent='space-between'>
            <Box onClick={() => markToDo(todo.todoId)} display="flex" alignItems='center' css={{ gap: '0.5rem' }}>
              <IconButton  variant='ghost' aria-label='Mark button' icon={<Icon as={todo.complete? MdCheckBox : MdCheckBoxOutlineBlank}/>} />
              <chakra.span as={todo.complete === true ? 's' : null} >{todo.title}</chakra.span>
            </Box>
            <Box>
              <IconButton onClick={() => RemoveToDo(todo.todoId)} variant='ghost' aria-label='Delete button' icon={<Icon as={MdDelete}/>} />
            </Box>
          </ListItem> 
        )
      })}
    </List>
  </Box>
  </>
  )
}
